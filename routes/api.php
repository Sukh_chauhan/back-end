<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('checkuser', 'Api\AuthController@user');
        Route::get('users', 'Api\AuthController@userList');
        Route::put('user/update', 'Api\AuthController@userUpdate');


    });
});


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'v1/admin'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::post('vehicle', 'Api\VehicleController@addVehicle');
        Route::get('vehicle/list', 'Api\VehicleController@getVehicles');

    });
});