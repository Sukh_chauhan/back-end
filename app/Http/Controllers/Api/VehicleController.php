<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\VehicleDetails;
use Exception;
use Carbon\Carbon;


class VehicleController extends Controller
{
    public $successStatus = 200;

    /**
     * Add Vehicle by admin token
     *
     * @param  [string] name
     * @param  [string] model
     * @param  [string] picture
     * @param  [string] color
     * @param  [integer] seatsbags
     * @param  [integer] bags
     * @param  [integer] amount_per_km
     * @param  [integer] amount_per_day
     * @return [string] message
     */
    public function addVehicle(Request $request)
    {
        $headers = apache_request_headers();
        if(!isset($headers['X-Requested-With']) || empty($headers['X-Requested-With'])){
            $error[]= "XML Http Request required";
         return response()->json(["error"=>$error], 404);   
        }
        $request->validate([
            'name' => 'required|string|min:3',
            'model' => 'required|min:2',
            'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'color' => 'required|string',
            'seats'=>'required|numeric',
            'bags'=>'required|numeric',
            'amount_per_km'=>'required|numeric',
            'amount_per_day'=>'required|numeric'

        ]);
        $input = $request->all();
        try
           {
            $imageName = time().'.'.$request->picture->extension();
            $request->picture->move(public_path('uploads/vehicles/'), $imageName);
            $input['picture'] = "uploads/vehicles/".$imageName;
            $VehicleDetails = VehicleDetails::create($input);
            return response()->json([
                'message' => 'Successfully added!'
            ], 201);

            } 
        catch(Exception $e)
            {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
            }
    }

    /**
     * Get the authenticated vehicles list
     *
     * @return [json] user object
     */
    public function getVehicles(Request $request)
    {
        $headers = apache_request_headers();
        if(!isset($headers['X-Requested-With']) || empty($headers['X-Requested-With'])){
            $error[]= "XML Http Request required";
         return response()->json(["error"=>$error], 404);   
        }
        try
           {
             $getVehicles = VehicleDetails::all()->toArray();
             $vehiclesListCount = VehicleDetails::all()->count();
             if($vehiclesListCount>0){
                 $data = array(); $count=0;
                 foreach ($getVehicles as $key => $value) {
                    $data[$count]['id'] = $value['id'];
                    $data[$count]['name'] = $value['name'];
                    $data[$count]['model'] = $value['model'];
                    $data[$count]['picture'] = $value['picture'];
                    $data[$count]['color'] = $value['color'];
                    $data[$count]['seats'] = $value['seats'];
                    $data[$count]['bags'] = $value['bags'];
                    $data[$count]['amount_per_km'] = $value['amount_per_km'];
                    $data[$count]['amount_per_day'] = $value['amount_per_day'];
                    $data[$count]['is_ac'] = $value['is_ac'] == 1 ? true : false;
                    $data[$count]['is_available'] = $value['is_available'] == 1 ? true : false;
                    $data[$count]['updated_at'] = Carbon::parse($value['updated_at'])->format('d-m-Y');

                    $count++;
                         
                 }
                 return response()->json( ['success'=> true, 'data' => $data], $this-> successStatus);

             } else {
                  $errors[] = "Data not exists!";
                  return response()->json(['error'=>$errors], 404);
             }
           } 
        catch(Exception $e)
           {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
           }
    }
}