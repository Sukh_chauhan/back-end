<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Exception;


class AuthController extends Controller
{
    public $successStatus = 200;
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {

        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone'=>'required|numeric',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            //'role'=>'required'
        ]);
        try
           {
                $user = new User([
                    'fname' => $request->first_name,
                    'lname' => $request->last_name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'role'=>'customer'
                ]);
                $user->save();
                return response()->json([
                    'success'=> true,
                    'message' => 'Successfully created user!'
                ], 201);
            }
        catch(Exception $e)
            {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
            }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        try
           {
                $user = $request->user();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();
                return response()->json([
                    'success'=> true,
                    'role'=>$user['role'],
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString()
                ]);
            }
        catch(Exception $e)
            {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
            }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $headers = apache_request_headers();
        if(!isset($headers['X-Requested-With']) || empty($headers['X-Requested-With'])){
            $error[]= "XML Http Request required";
         return response()->json(["error"=>$error], 404);
        }
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $headers = apache_request_headers();
        if(!isset($headers['X-Requested-With']) || empty($headers['X-Requested-With'])){
            $error[]= "XML Http Request required";
         return response()->json(["error"=>$error], 404);
        }
        try {
            $userDetails = $request->user();
            $data=array();
            if(!empty($userDetails) && $userDetails['is_active']== 1){
                $data= array("id"=>$userDetails['id'],
                "fname"=>$userDetails['fname'],
                "lname"=>$userDetails['lname'],
                "role"=>$userDetails['role'],
                "phone"=>$userDetails['phone'],
                "email"=>$userDetails['email'],
                );
                return response()->json( ['success'=> true, 'data' => $data], $this-> successStatus);
            }
            else {
              $errors[] = "Data not found!";
              return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 404);
            }
           }
        catch(Exception $e)
           {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
           }

    }

    /**
     * Update the authenticated User
     *
     * @return [json] user object
     */
    public function userUpdate(Request $request)
    {
        $headers = apache_request_headers();
        if(!isset($headers['X-Requested-With']) || empty($headers['X-Requested-With'])){
            $error[]= "XML Http Request required";
         return response()->json(["error"=>$error], 404);
        }
        try {
            $userDetails = $request->user();
            $data=array();
            if(!empty($userDetails) && $userDetails['is_active']== 1){

                /*$data= array("id"=>$userDetails['id'],
                "fname"=>$userDetails['fname'],
                "lname"=>$userDetails['lname'],
                "role"=>$userDetails['role'],
                "phone"=>$userDetails['phone'],
                "email"=>$userDetails['email'],
                );*/
                return response()->json( ['success'=> true, 'data' => $data], $this-> successStatus);
            }
            else {
              $errors[] = "Data not found!";
              return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 404);
            }
           }
        catch(Exception $e)
           {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
           }

    }

    /**
     * Get the authenticated User List
     *
     * @return [json] user object
     */
    public function userList(Request $request)
    {
        $headers = apache_request_headers();
        if(!isset($headers['X-Requested-With']) || empty($headers['X-Requested-With'])){
            $error[]= "XML Http Request required";
         return response()->json(["error"=>$error], 404);
        }
        try
           {
            $users = User::where(['role'=>'customer'])->get()->toArray();
            $usersListCount = User::where('role','customer')->get()->count();;
             if($usersListCount>0){
                 $data = array(); $count=0;
                 foreach ($users as $key => $value) {
                    $data[$count]['id'] = $value['id'];
                    $data[$count]['fname'] = $value['fname'];
                    $data[$count]['lname'] = $value['lname'];
                    $data[$count]['phone'] = $value['phone'];
                    $data[$count]['email'] = $value['email'];
                    $data[$count]['is_active'] = $value['is_active'] == 1 ? true : false;
                    $data[$count]['updated_at'] = Carbon::parse($value['updated_at'])->format('d-m-Y');

                    $count++;

                 }
                 return response()->json( ['success'=> true, 'data' => $data], $this-> successStatus);

             } else {
                  $errors[] = "Data not exists!";
                  return response()->json(['error'=>$errors], 404);
             }
           }
        catch(Exception $e)
           {
                $data = $e->getMessage();
                $errors[] = "Please try again!";
                return response()->json(['success'=> false, 'data' => $data ,'error'=>$errors], 401);
           }
    }



}
