<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleDetails extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'model', 'picture', 'color', 'seats', 'bags', 'amount_per_km', 'amount_per_day'
    ];
}
