<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_details', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->string('model',100);
            $table->string('picture',255);
            $table->string('color',20)->nullable();
            $table->integer('seats');
            $table->integer('bags');
            $table->double('amount_per_km', 8, 2);
            $table->double('amount_per_day', 8, 2);
            $table->boolean('is_ac')->default(1);
            $table->boolean('is_available')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_details');
    }
}
